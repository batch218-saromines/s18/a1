// 1.  Create a function which will be able to add two numbers.
// 		-Numbers must be provided as arguments.
// 		-Display the result of the addition in our console.
// 		-function should only display result. It should not return anything.


	// addition


	function sum(addNumber1,addNumber2){
		console.log("Displayed sum of " + addNumber1 + " and " + addNumber2)
	}
	sum(addNumber1=5,addNumber2=15)

	let num1=5;
	let num2=15;

	let sumValue=num1+num2

	console.log(sumValue)



// Create a function which will be able to subtract two numbers.
// 	-Numbers must be provided as arguments.
// 	-Display the result of subtraction in our console.
// 	-function should only display result. It should not return anything.



	// subtraction

	// function subtractNumber(num1, num2){
	// 	console.log("Displayed difference of " + num1 + " and " + num2)
	// 	return num1 - num2;
	// }

	// let differenceValue = subtractNumber (20, 5);
	// 	console.log(differenceValue);


	function difference(subtractNumberA,subtractNumberB){
		console.log("Displayed difference of " + subtractNumberA + " and " + subtractNumberB)
	}
	difference(subtractNumberA=20,subtractNumberB=5)

	let numA=20;
	let numB=5;

	let differenceValue=numA-numB

	console.log(differenceValue)




// 	-invoke and pass 2 arguments to the addition function
// 	-invoke and pass 2 arguments to the subtraction function



// 2. Create a function which will be able to multiply two numbers.
// 			-Numbers must be provided as arguments.
// 			-Return the result of the multiplication.



	function multiplyNumber(num1, num2){
		console.log("The product of " + num1 + " and " + num2 + ':')
		return num1 * num2;
	}

	let productValue = multiplyNumber(50, 10);
	console.log(productValue);



// Create a function which will be able to divide two numbers.
// 	-Numbers must be provided as arguments.
// 	-Return the result of the division.



	function divideNumber(num1, num2){
		console.log("The quotient of " + num1 + " and " + num2 + ":")
		return num1 / num2;
	}

		let quotientValue = divideNumber(50 , 10);
		console.log(quotientValue);



// Create a global variable called outside of the function called product.
// 		-This product variable should be able to receive and store the result of multiplication function.
// Create a global variable called outside of the function called quotient.
// 		-This quotient variable should be able to receive and store the result of division function.


// 3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
// 			-a number should be provided as an argument.
// 			-look up the formula for calculating the area of a circle with a provided/given radius.
// 			-look up the use of the exponent operator.
// 			-you can save the value of the calculation in a variable.
// 			-return the result of the area calculation.

// 		Create a global variable called outside of the function called circleArea.
// 			-This variable should be able to receive and store the result of the circle area calculation.

// 	Log the value of the circleArea variable in the console.



	function circleArea(num1, num2){
		console.log ("The result of getting the area of a circle with 15 radius: ");

		let numberArea = (num1 * (num2 * num2));

		return numberArea;

	}
		let areaCircle = circleArea(3.1416, 15);
		console.log(areaCircle);




// 4. 	Create a function which will be able to get total average of four numbers.
// 		-4 numbers should be provided as an argument.
// 		-look up the formula for calculating the average of numbers.
// 		-you can save the value of the calculation in a variable.
// 		-return the result of the average calculation.

//     Create a global variable called outside of the function called averageVar.
// 		-This variable should be able to receive and store the result of the average calculation
// 		-Log the value of the averageVar variable in the console.



	function totalAverage(no1, no2, no3, no4){
		console.log ("The average of 20,40,60 and 80:");

	let AverageNumber = ((no1 + no2 + no3 + no4) / 4);
		return AverageNumber;
	}
	let averageVar = totalAverage(20, 40, 60, 80);
	console.log(averageVar);



// 5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
// 		-this function should take 2 numbers as an argument, your score and the total score.
// 		-First, get the percentage of your score against the total. You can look up the formula to get percentage.
// 		-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
// 		-return the value of the variable isPassed.
// 		-This function should return a boolean.

// 	Create a global variable called outside of the function called isPassingScore.
// 		-This variable should be able to receive and store the boolean result of the checker function.
// 		-Log the value of the isPassingScore variable in the console.



	function isPassingScore(num){

	let score = "38";
	let total = "50"
	let passingScore = score >= 30;
	console.log("Is " + score + " / " + total +" a passing score?");
	console.log(passingScore);
	}
	isPassingScore(38,50);

